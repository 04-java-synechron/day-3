package com.classpath.java8.streams.model;

import java.time.LocalDate;

public class Student implements Comparable<Student>{
    private String name;
    private int marks;
    private LocalDate dob;

    public Student(String name, int marks){
        this.name = name;
        this.marks = marks;
    }
    public Student(String name, int marks, LocalDate dob){
        this.name = name;
        this.marks = marks;
        this.dob = dob;
    }

    public String getName(){
        return this.name;
    }

    public int getMarks(){
        return this.marks;
    }
    public LocalDate getDob(){
        return this.dob;
    }


    @Override
    public int compareTo(Student student) {
        return this.marks - student.marks;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", marks=" + marks +
                '}';
    }
}