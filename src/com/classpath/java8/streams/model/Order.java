package com.classpath.java8.streams.model;

import java.util.Objects;

public class Order {

    private long id;
    private String customerEmail;
    private double price;

    public Order(long id, String customerEmail, double price) {
        this.id = id;
        this.customerEmail = customerEmail;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                Objects.equals(customerEmail, order.customerEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customerEmail);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customerEmail='" + customerEmail + '\'' +
                ", price=" + price +
                '}';
    }
}