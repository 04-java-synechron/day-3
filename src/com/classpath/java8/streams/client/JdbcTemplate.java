package com.classpath.java8.streams.client;

public abstract  class JdbcTemplate {

    private final String url;
    private final String username;
    private final String password;

    private DBConnection dbConnection;

    public JdbcTemplate(String url, String username, String password){
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public void execute (){
        //step-1
        acquireDBConnection();
        //step-2
        createStatement();
        //step-3
        executeStatement();
        //step-4
        processData();
        //step-5
        closeStatement();
        //step-6
        closeConnection();
    }

    private void closeConnection() {
        System.out.println("closing the DB connection");
    }

    private void closeStatement() {
        System.out.println("closing the statement");
        this.dbConnection.releaseDBConnection();
    }

    public abstract void processData();

    public abstract void executeStatement();

    public abstract void createStatement();

    private void acquireDBConnection() {
        this.dbConnection = new DBConnection(this.url, this.username, this.password);
        System.out.println("Creating the DB connection");
    }
}

class DBConnection {
    private final String url;
    private final String username;
    private final String password;

    public DBConnection(String url, String username, String password){
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public void releaseDBConnection(){

    }
}