package com.classpath.java8.streams.client;

import com.classpath.java8.streams.model.Student;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class SteamsAPIDemo {

    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
                new Student("Ramesh", 33, LocalDate.of(1983, 10, 12)),
                new Student("Suresh", 34, LocalDate.of(1983, 10, 10)),
                new Student("Vinay", 14, LocalDate.of(1985, 12, 14)),
                new Student("Kishore", 24, LocalDate.now()));

        /*Optional<String> fetchedStudent = findFirstStudent(students);
        if(fetchedStudent.isPresent()){
            String studentName = fetchedStudent.get();
            System.out.println("Name of the student is "+ studentName);
        }*/
        String name = findFirstStudent(students)
                //        .orElseThrow(() -> new IllegalArgumentException(" Invalid data passed"));
                          //.orElse("Ramesh");
                          //  .orElseGet(() -> UUID.randomUUID().toString());
                            .orElseThrow();

        //System.out.println(name);
        printTotalMarksForAllStudents(students);

    }

    public static final Optional<String> findFirstStudent(List<Student> students){
        Comparator<Student> compareByNameAndDate = Comparator.comparing(Student::getDob, LocalDate::compareTo).thenComparing(Student :: getName, String::compareToIgnoreCase);


        Optional<String> fetchedStudent = students.stream()
                .sorted(compareByNameAndDate)
                .map(Student::getName)
                .findFirst();
        return  fetchedStudent;

    }

    public static final void printTotalMarksForAllStudents(List<Student> students){

        Integer totalMarks = students
                .stream()
                .map(Student::getMarks)
                // .forEach(System.out::println);
                //.collect(Collectors.toCollection())
                .reduce(0, Integer::sum);

        System.out.println(" Total marks is "+ totalMarks);
    }
}