package com.classpath.java8.streams.client;

public class MySQLJdbcTemplate extends JdbcTemplate{

    public MySQLJdbcTemplate(String url, String username, String password) {
        super(url, username, password);
    }

    @Override
    public void processData() {
        System.out.println(" Processing the data");
    }

    @Override
    public void executeStatement() {
        System.out.println("executing the statement");
    }

    @Override
    public void createStatement() {
        System.out.println("Creaing SQL statment");
    }

    public static void main(String[] args) {
        JdbcTemplate template = new MySQLJdbcTemplate("jdbc:mysql", "root", "welcome");
        template.execute();
    }
}