package com.classpath.java8.streams.client;

import com.classpath.java8.streams.model.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class StudentTemplateParserDemo {

    static List<Student> finalList = new ArrayList<>();

    public static void main(String[] args) {
        processStudent(new Student("Umesh", 55), (student -> finalList.add(student)));
    }

    //template design pattern
    public static void processStudent(Student student, Consumer<Student> mapStudent) {
        //step-1
        //step-2
        boolean flag = validateStudent(student);
        if (flag) {
            mapStudent.accept(student);
        }
        //step-4
        //step-5
    }

    private static boolean validateStudent(Student student) {
        return student.getMarks() > 45;
    }
}