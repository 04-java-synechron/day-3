package com.classpath.java8.streams.client;

import com.classpath.java8.streams.model.Order;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class StreamsDemo {

    public static void main(String[] args) {
        List<Order> orders = Arrays.asList(
            new Order(12, "vinay@gmail.com", 45_00_000),
            new Order(22, "harish@gmail.com", 5_40_000),
            new Order(41, "suryansh@gmail.com", 12_40_000),
            new Order(56, "gopi@gmail.com", 35_00_000),
            new Order(67, "jeevan@gmail.com", 25_00_000)
        );

        Predicate<Order> ordersLessThan25K = (order) -> order.getPrice() < 25_00_000 ;
        Predicate<Order> ordersMoreThan25K = ordersLessThan25K.negate() ;

        Consumer<Order> orderConsumer = (order) -> System.out.println(order);

        Function<Order, Double> orderToPrice = (order) -> order.getPrice();

        Consumer<Double> orderValueConsumer = (price) -> System.out.println(price);

        processOrders(orders, ordersMoreThan25K, orderToPrice, orderValueConsumer);

    }

    public static void processOrders (List<Order> input, Predicate<Order> orderPredicate, Function<Order, Double> orderDoubleFunction, Consumer<Double> orderConsumer){
        input
                .stream()
                .filter(orderPredicate)
                .map(orderDoubleFunction)
                .map(value -> "The price of the Order is "+ value)
                .forEach(output -> System.out.println(output));

    }
}