package com.classpath.java8.streams.client;

import com.classpath.java8.streams.model.Student;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueDemo {

    public static void main(String[] args) {
        Queue<Student> queue = new PriorityQueue<>((student1, student2) -> student2.getName().compareTo(student1.getName()));
        queue.add(new Student("Ramesh", 33));
        queue.add(new Student("Suresh", 34));
        queue.add(new Student("Vinay", 14));
        queue.add(new Student("Kishore", 24));

        for(Student student: queue){
            System.out.println(student);
        }
        /*System.out.println("Size of the queue is "+ queue.size());
        System.out.println(queue.peek());
        System.out.println(queue.peek());
        System.out.println(queue.peek());
        System.out.println("Size of the queue is "+ queue.size());*/

    }
}

