package com.classpath.java8.streams.client;

import com.classpath.java8.streams.model.Student;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
    public static void main(String[] args) {
        Set<Student> students = new TreeSet<>((student1, student2) -> student2.getName().compareTo(student1.getName()));

        students.add(new Student("Ramesh", 33));
        students.add(new Student("Suresh", 34));
        students.add(new Student("Vinay", 14));
        students.add(new Student("Kishore", 24));

        for(Student student: students){
            System.out.println(student);
        }
    }

}