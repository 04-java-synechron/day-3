package com.classpath.java8.streams.flatmap;

import java.util.Arrays;
import java.util.OptionalDouble;

public class DuplicateRemovalDemo {

    public static void main(String[] args) {
        int[][] array = {{2,2}, {3,4}, {5,3}, {7,3}};
        OptionalDouble average = Arrays.stream(array)
                .flatMapToInt(Arrays::stream)
                .distinct()
                .average();

        System.out.println(average);

    }
}