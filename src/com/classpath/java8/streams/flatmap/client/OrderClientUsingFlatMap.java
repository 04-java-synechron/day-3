package com.classpath.java8.streams.flatmap.client;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.*;

public class OrderClientUsingFlatMap {

    public static void main(String[] args) {
        LineItem iphone = new LineItem("iphone", 75000, 2);
        LineItem thinkpad = new LineItem("Thinkpad", 125000, 1);
        LineItem keyboard = new LineItem("keyboard", 4000, 2);
        Order order1 = new Order(1234, "pradeep@gmail.com");
        order1.setLineItems(asList(iphone, thinkpad, keyboard));

        LineItem ipad = new LineItem("ipad", 35000, 2);
        LineItem macbookPro = new LineItem("Macbook-pro", 225000, 1);
        LineItem mouse = new LineItem("mouse", 4000, 2);
        Order order2 = new Order(1235, "ramesh@gmail.com");
        order2.setLineItems(asList(iphone, thinkpad, keyboard));

//        double sum = order.getLineItems().stream().mapToDouble(LineItem::getPrice).sum();
//        System.out.println("Total value of order is "+ sum);

        List<Order> orderList = Arrays.asList(order1, order2);

        Stream<List<Order>> orderListStream = Stream.of(orderList);

        double totalValueFromAllOrders = orderListStream
                .flatMap(orders -> orders.stream())
                .map(order -> order.getLineItems())
                .flatMap(lineItems -> lineItems.stream())
                .mapToDouble(lineItem -> lineItem.getPrice())
                .sum();



    }
}