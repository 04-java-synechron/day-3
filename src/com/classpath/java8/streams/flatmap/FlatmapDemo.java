package com.classpath.java8.streams.flatmap;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FlatmapDemo {

    public static void main(String[] args) {
        // {{1,2}, {3,4}, {5,6}, {7,8}}
        // {1,2,3,4,5,6,7,8}

        int[][] array = {{1,2}, {3,4}, {5,4}, {7,3}};

        Stream<int[]> arrayStream = Arrays.stream(array);
        //Stream<int[]> -> Stream<int>
        //arrayStream.forEach(input -> System.out.println(input));

        /*List<int[]> outputStream = arrayStream
                                        .filter(input -> {
                                                    for (int number : input) {
                                                        if (number == 7) {
                                                            return false;
                                                        }
                                                    }
                                                    return true;
                                     }).collect(Collectors.toList());
        */

      //System.out.println(outputStream);


      //  Integer[] integers = arrayStream.flatMap(input -> Stream.of(input)).toArray(Integer[]::new);
        // IntStream intStream = arrayStream.flatMapToInt((arr) -> Arrays.stream(arr));
        //System.out.println(Arrays.stream(integers).reduce(0, Integer:: max));

        Stream<int[]> stream1 = Stream.of(array);
        Stream<int[]> stream2 = Arrays.stream(array);
        Stream<int[]> stream3 = Arrays.asList(array).stream();

        IntStream streamOfIntegers = stream1.flatMapToInt(a -> Arrays.stream(a));
        OptionalInt maxValue = streamOfIntegers.reduce(Integer::max);
        System.out.println("Maximum value is "+ maxValue);


        /*
           Stream<Integer[]>   => flatMap() => Stream<Integer>
           Stream<List<String>> => flatMap() => Stream<String>

         */

    }
}