package com.classpath.java8.streams.demo;

import com.classpath.java8.streams.model.Order;
import com.classpath.java8.streams.model.Student;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Stream;

public class FlatMap {

    public static void main(String[] args) {

        Order[] orders = {new Order(12, "vinay@gmail.com", 45_00),
                new Order(22, "harish@gmail.com", 540),
                new Order(41, "suryansh@gmail.com", 12_400),
                new Order(56, "gopi@gmail.com", 35_000),
                new Order(67, "jeevan@gmail.com", 2_500)};

        Stream<Order> orderStream = Arrays.stream(orders);

        //Stream<Order> orderStreamLessThan25K = orderStream.filter(order -> order.getPrice() < 25_00_000);
        //Stream<String> emailWithOrdersLessThan25K = orderStreamLessThan25K.map(Order :: getCustomerEmail);
        //double totalOrder = orderStream.map(Order::getPrice).reduce(0.0, Double::sum);
        //System.out.println("Total order amount value is "+ totalOrder);
/*
        orderStream
                .max((order1, order2) -> (int)(order1.getPrice() - order2.getPrice()))
                .map(order -> order.getCustomerEmail())
                .ifPresent(System.out:: println);
*/

        orderStream
                .sorted(Comparator.comparingDouble(Order::getPrice))
                .skip(1)
                //.limit(1)
                .findFirst()
                .ifPresent(System.out::println);


    }
}